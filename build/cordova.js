'use strict'
const exec = require('child-process-promise').exec;
const fs = require('fs-extra');

console.log("--Cordova--");

// add const pluginsList to add cordova plugin like
const pluginsList = {
    "Cordova Plugin BardCodeScanner" : "phonegap-plugin-barcodescanner",
    "Cordova Plugin Camera" : "cordova-plugin-camera",
    "Cordova Plugin Fullscreen" : "cordova-plugin-fullscreen",
    "Cordova NFC Plugin" : "phonegap-nfc",
    "Cordova Plugin File" : "cordova-plugin-file",
    "Cordova Plugin FileTransfer" : "cordova-plugin-file-transfer",
    "Cordova File Opener Plugin" : "cordova-plugin-file-opener2",
    "Cordova Plugin Local Notification" : "cordova-plugin-local-notification",
    "Cordova Android Support Gradle Release" : "cordova-android-support-gradle-release",
    "Cordova Plugin Ionic Keyboard" : "ionic-plugin-keyboard"
};

if (!fs.existsSync('cordova')) {
    console.log("Cordova app doesn't exist, create project");
    exec("cordova create cordova itb.mochi TTK && cd cordova && cordova platform add android")
    .then(result => {
        console.log("Cordova app created");
        copy_project().then(result => {
            fs.copy("build/res", "cordova/res")
            .then(result => {
                install_plugins()
                .then(result => {
                    console.log('Finished !');
                })
            })
        })
    })
}
else {
    console.log("Cordova app exist, update project");
    copy_project()
    .then(result => {
         console.log('Finished !');
    })
}

function install_plugins() {
    let pluginsPromise = [];

    if (pluginsList.length > 0) {
        console.log("Install plugins");
    }

    Object.keys(pluginsList).map(function(objectKey, index) {
        let value = pluginsList[objectKey];
        pluginsPromise.push(
            exec("cd cordova && cordova plugin add " + value)
            .then(result => {
                console.log(objectKey + ' installed!');
            })
        );
    });

    return Promise.all(pluginsPromise);
}


function copy_project() {
    return fs.remove('cordova/www/')
    .then(result => {
        fs.mkdir('cordova/www')
            .then(result => {
            console.log("Copy project");
            fs.copy('dist', 'cordova/www/')
                .then(result => {
                    fs.copy("build/cordova-config.xml","cordova/config.xml")
                        .then(result => {
                            console.log("Finish to copy project")
                        })
                })
        });
    })
}
