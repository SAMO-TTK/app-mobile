'use strict'
module.exports = {
    NODE_ENV: '"production"',
    API: '"http://api-ttk.dev.itineraire-b.com/api"',
    STORAGE: '"http://api-ttk.dev.itineraire-b.com/storage"',
    WANA : '"http://wkrest.toutenkamion.com:81"',
    MFILES: '"https://mfiles.toutenkamion.com/REST"',
    VIEWMFILES: "'https://mfiles.toutenkamion.com/Default.aspx?#7061608C-319D-43C7-9017-6D940C3731C7/object'",
}
