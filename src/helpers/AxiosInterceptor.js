import axios from 'axios';
import store from '../store';

export default function setup() {
    //set dynamically the good authorization key in the header for mfiles or own api
    axios.interceptors.request.use(function (config) {
        if (store !== undefined && store.state.auth.token) {
            if (store.state.auth.mfiles !== null && typeof store.state.auth.mfiles === "string" && config.url.match(/mfiles/)) {
                config.headers['X-Authentication'] = `${store.state.auth.mfiles}`;
            }
            else {
                config.headers['Authorization'] = `Bearer ${store.state.auth.token}`;
            }
        }

        return config;
    });
}