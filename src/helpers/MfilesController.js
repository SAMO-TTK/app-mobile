import Vue from 'vue';
import store from '../store';
export class MfilesController {
	constructor(){
		this.tmp_response_files = {};
	}
	delete_file(file, callback){
//?allVersions=true
		Vue.axios.put(`${process.env.MFILES}/objects/0/${file.objectId}/deleted`,JSON.stringify({
			value:1
		})).then(response => {

			callback();
		}).catch(() =>{

		});
	}
	update_name(id, name, callback){
		Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/checkedout`,JSON.stringify({
			value:1
		})).then(() => {
			Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/properties/0`, JSON.stringify({

				PropertyDef: 0,
				TypedValue: {
					DataType: 1,
					Value: name
				}
			})).then(() => {
				Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/checkedout`, JSON.stringify({
					value:0
				})).then(() => {
					callback();
				})
			})
		})
	}

	openDocument(selected, url, soft){
		const electron = window.require('electron');
		const http = window.require('http');
		const https = window.require('https');
		const fs   = window.require('fs');
		const {shell} = window.require('electron');
		const path = window.require('path');
		const remote = electron.remote;
		const app  = remote.app;
		fs.readdir(app.getPath('documents') + '/ttk/', (err, files) => {
			for(file in files){
				fs.unlink(app.getPath('documents') + '/ttk/'+files[file])
			}
			fs.rmdir(app.getPath('documents') + '/ttk/');
			fs.mkdir(app.getPath('documents') + '/ttk/');
			var file = fs.createWriteStream(app.getPath('documents') +  '/ttk/' + selected.name + '.' + selected.extension);

			if(url.match(/https:/)){

				var request = https.get(url, response => {

					response.pipe(file);

					response.on('end', () => {
						this.openFile(app.getPath('documents') + '/ttk/' + selected.name + '.' + selected.extension, {app: soft})
					});
				});
			}else {

				var request = http.get(url, response => {

					response.pipe(file);

					response.on('end', () => {
						this.openFile(app.getPath('documents') + '/ttk/' + selected.name + '.' + selected.extension, {app :soft})
					});
				});
			}
		})
	}

	openFile(target, opts) {
		const path = window.require('path');
		const childProcess = window.require('child_process');
		opts = Object.assign({wait: true}, opts);
		let cmd;
		let appArgs = [];
		let args = [];
		const cpOpts = {};

		if (Array.isArray(opts.app)) {
			appArgs = opts.app.slice(1);
			opts.app = opts.app[0];
		}

		if (window.process.platform === 'darwin') {
			cmd = 'open';

			if (opts.wait) {
				args.push('-W');
			}

			if (opts.app) {
				args.push('-a', opts.app);
			}
		} else if (window.process.platform === 'win32') {
			cmd = 'cmd';
			args.push('/c', 'start', '""', '/b');
			target = target.replace(/&/g, '^&');

			if (opts.wait) {
				args.push('/wait');
			}

			if (opts.app) {
				args.push(opts.app);
			}

			if (appArgs.length > 0) {
				args = args.concat(appArgs);
			}
		} else {
			if (opts.app) {
				cmd = opts.app;
			} else {
				cmd = path.join(__dirname, 'xdg-open');
			}

			if (appArgs.length > 0) {
				args = args.concat(appArgs);
			}

			if (!opts.wait) {
				// `xdg-open` will block the process unless
				// stdio is ignored and it's detached from the parent
				// even if it's unref'd
				cpOpts.stdio = 'ignore';
				cpOpts.detached = true;
			}
		}


		args.push(target);

		if (window.process.platform === 'darwin' && appArgs.length > 0) {
			args.push('--args');
			args = args.concat(appArgs);
		}

		const cp = childProcess.spawn(cmd, args, cpOpts);

		// if (opts.wait) {
		// 	return new Promise((resolve, reject) => {
		// 		cp.once('error', reject);
		//
		// 		cp.once('close', code => {
		// 			if (code > 0) {
		// 				reject(new Error('Exited with code ' + code));
		// 				return;
		// 			}
		//
		// 			resolve(cp);
		// 		});
		// 	});
		// }
		/** Send the file to MFiles
		 * @param {string} fileURL : The emplacement of the file in the device (referer to Cordova file directory)
		 * */
	}
	post_file(fileURL)
	{

		return new Promise(resolve => {
			let options = new FileUploadOptions();//create new upload options
			options.fileKey = "file";
			options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
			options.mimeType = "application/octet-stream";
			options.headers = {
				'X-Authentication': store.state.auth.mfiles//set mfiles token
			};

			let ft = new FileTransfer();//use file transfer cordova plugin
			//upload the file to mfiles
			ft.upload(
				fileURL,
				encodeURI(`${process.env.MFILES}/files`),
				(response) => {
					//remove the file after is uploaded
					resolveLocalFileSystemURL(fileURL, function (entry) {
						entry.remove();
					});
					resolve(JSON.parse(response.response));
				},
				(e) => {
					console.log('error', e);
				},
				options);//upload the file
		});
	}
	post_object(file, kamion, callback, name, d1, d2, d0)
	{
		this.post_file(file).then(reponse_file => {
			Vue.axios.post(`${process.env.MFILES}/objects/0`, this.data_kamion(reponse_file, kamion, name, d1, d2, d0)).then(response => {
				callback(response.data, file);
			}).catch(response => {
				console.log('error', response);
			})
		});
	}
	data_kamion(reponse_file, kamion, name, d1, d2, d0)
	{
		return {
      PropertyValues: [
        {
          // workflow
          PropertyDef: 38,
          TypedValue: {
            DataType: 9,
            Lookup: {
              Item: 113
            }
          }
        },
        {
          // Document name
          PropertyDef: 0,
          TypedValue: {
            DataType: 1,
            Value: reponse_file.Title
          }
        },
        {
          // "Single File" property
          PropertyDef: 22,
          TypedValue: {
            DataType: 8,
            Value: true
          }
        },
        {
          // "KAMION" property
          PropertyDef: 1036,
          TypedValue: {
            DataType: 10,
            Lookups: [{
              Item: kamion.id * 1
            }]
          }
        },
        {
          // "PHASE" property
          PropertyDef: 1032,
          TypedValue: {
            DataType: 10,
            Lookups: [{
              Item: d0
            }]
          }
        },
        {
          // "TYPE DE DOCUMENT" property
          PropertyDef: 1020,
          TypedValue: {
            DataType: 10,
            Lookups: [{
              Item: d1
            }]
          }
        },
        {
          // "NATURE" property
          PropertyDef: 1026,
          TypedValue: {
            DataType: 10,
            Lookups: [{
              Item: d2
            }]
          }
        },
        {
          // CLASSE
          PropertyDef: 100,
          TypedValue: {
            DataType: 9,
            Lookup: {
              Item: 5
            }
          }
        },
        {
          // obsolette
          PropertyDef: 1155,
          TypedValue: {
            DataType: 9,
            Lookup: {
              Item: 2
            }
          }
        }],
      Files: [reponse_file]
    };
	}
	update_path(id, d1, d2, callback)
	{
		Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/checkedout`, JSON.stringify({
			value: 1
		})).then(() => {
			Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/properties/1020`, JSON.stringify({

				PropertyDef: 1020,
				TypedValue: {
					DataType: 10,
					Lookups: [{
						Item: d1
					}]
				}
			})).then(() => {
				if (d2) {
					Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/properties/1026`, JSON.stringify({

						PropertyDef: 1026,
						TypedValue: {
							DataType: 10,
							Lookups: [{
								Item: d2
							}]
						}
					})).then(() => {
						Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/checkedout`, JSON.stringify({
							value: 0
						})).then(() => {
							callback();
						})
					})
				} else {

					Vue.axios.put(`${process.env.MFILES}/objects/0/${id}/latest/checkedout`, JSON.stringify({
						value: 0
					})).then(() => {
						callback();
					})
				}
			})
		})
	}
}
