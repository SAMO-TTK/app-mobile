import Vue          from 'vue'
import App          from './App'
import { router }   from './router'
import store        from './store'
import axios        from 'axios'
import VueAxios     from 'vue-axios'
import Vuetify      from 'vuetify'
import _            from 'lodash'
import AxiosInterceptor from './helpers/AxiosInterceptor';

Vue.use(VueAxios, axios);
Vue.use(Vuetify);


Vue.config.productionTip = false;

Vue.prototype.$eventHub = new Vue();

Vue.prototype.$_ = _;

AxiosInterceptor();

new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: { App }
});

