import store from '../../store'

export default {
    isConnected(to, from, next) {
        store.dispatch('loadUserState')

        if(to.path === '/auth/login' && store.state.auth.isConnected) {
            next({ path: '/' });

        } else if (to.path === '/auth/login' || store.state.auth.isConnected) {
            next();

        } else {
            next({ path: '/auth/login' });
        }
    }
}
