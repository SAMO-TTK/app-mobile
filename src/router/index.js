import Vue from 'vue'
import VueRouter from 'vue-router'
import Auth from './filters/auth'

Vue.use(VueRouter)

export const router = new VueRouter({
    mode: (!!window.cordova) ? 'hash' : 'history',

    routes: [
        {
            path: '/',
            redirect: {name: 'list-kamions'},
            component: require('../layouts/app/Layout.vue').default,

            children: [
                //#region - kamion
                {
                    path: 'kamions',
                    name: 'list-kamions',
                    components: {
                        app_content: require('../views/kamions/list/List.vue').default,
                    }
                },
                {
                    path: 'kamions/:id',
                    name: 'single-kamion',
                    redirect: {name: 'single-kamion-informations'},
                    components: {
                        app_content: require('../views/kamions/single/Single.vue').default
                    },
                    children: [
                        {
                            path: 'informations',
                            name: 'single-kamion-informations',
                            components: {
                                default: require('../views/kamions/single/Informations.vue').default
                            }
                        },
                        {
                            path: 'informations-complementaires',
                            name: 'single-kamion-complementary-informations',
                            components: {
                                default: require('../views/kamions/single/ComplementaryInformations.vue').default
                            }
                        },
                        {
                            path: 'instructions',
                            name: 'single-kamion-instructions',
                            components: {
                                default: require('../views/kamions/single/Instructions.vue').default
                            }
                        }
                    ]
                },
                //#endregion
                //#region - documents
                {
                    path: 'documents/:tab?/:folder?/:subFolder?',
                    name: 'list-documents',
                    components: {
                        app_content: require('../views/documents/list/List.vue').default,
                    }
                },
                {
                    path: 'documents/edit/informations',
                    name: 'select-document-informations',
                    components: {
                        app_content: require('../views/documents/edit/SelectInformations.vue').default
                    }
                },
                //#endregion
                //#region - logtimes
                {
                    path: 'temps/:status?',
                    name: 'list-logtimes',
                    components: {
                        app_content: require('../views/logtimes/list/List.vue').default,
                    },
                },
                {
                    path: 'temps/:status/:id',
                    name: 'single-logtimes',
                    components: {
                        app_content: require('../views/logtimes/single/Single.vue').default
                    },
                },
                {
                    path: 'temps/edit/one/kamion',
                    name: 'select-logtime-kamion',
                    components: {
                        app_content: require('../views/logtimes/edit/SelectKamion.vue').default
                    }
                },
                {
                    path: 'temps/edit/one/instruction',
                    name: 'select-logtime-instruction',
                    components: {
                        app_content: require('../views/logtimes/edit/SelectInstruction.vue').default
                    }
                },
                {
                    path: 'temps/edit/one/informations',
                    name: 'select-logtime-informations',
                    components: {
                        app_content: require('../views/logtimes/edit/SelectInformations.vue').default
                    }
                },
                //#endregion
                //#region - materials
                {
                    path: 'matieres/:status?',
                    name: 'list-materials',
                    components: {
                        app_content: require('../views/materials/list/List.vue').default,
                    },
                },
                {
                    path: 'matieres/:status/:id',
                    name: 'single-material',
                    components: {
                        app_content: require('../views/materials/single/Single.vue').default
                    },
                },
                {
                    path: 'matieres/edit/one/kamion',
                    name: 'select-material-kamion',
                    components: {
                        app_content: require('../views/materials/edit/SelectKamion.vue').default
                    }
                },
                {
                    path: 'matieres/edit/one/instruction',
                    name: 'select-material-instruction',
                    components: {
                        app_content: require('../views/materials/edit/SelectInstruction.vue').default
                    }
                },
                {
                    path: 'matieres/edit/one/ressources-matiere',
                    name: 'select-material-materialresource',
                    components: {
                        app_content: require('../views/materials/edit/SelectMaterialResources.vue').default
                    }
                },
                {
                    path: 'matieres/edit/choose/informations',
                    name: 'select-material-informations',
                    components: {
                        app_content: require('../views/materials/edit/SelectInformations.vue').default
                    }
                },
                //#endregion
                //#region - notifications
                {
                    path: 'notifications',
                    name: 'list-notifications',
                    components: {
                        app_content: require('../views/notifications/list/List.vue').default,
                    },
                },
                //#endregion
                //#region - pathofexpectations
                /*{
                    path: 'chemin-des-attentes',
                    name: 'pathOfExpectations',
                    components: {
                        app_content: require('../views/elements/pathOfExpectations/PathOfExpectations.vue').default,
                    }
                },
                {
                    path: 'elements/:id',
                    name: 'single-elements',
                    redirect: {name: 'single-elements-informations'},
                    components: {
                        app_content: require('../views/elements/single/Single.vue').default
                    },
                    children: [
                        {
                            path: 'informations',
                            name: 'single-elements-informations',
                            components: {
                                default: require('../views/elements/single/Informations.vue').default
                            }
                        },
                        {
                            path: 'debats',
                            name: 'single-elements-debates',
                            components: {
                                default: require('../views/elements/single/Debates.vue').default
                            }
                        },
                        {
                            path: 'fichiers-client',
                            name: 'single-elements-client-files',
                            components: {
                                default: require('../views/elements/single/ClientFiles.vue').default
                            }
                        }
                    ]
                },*/
                //#endregion
                //#region - debates
                /*{
                    path: 'debats/:id',
                    name: 'single-debates',
                    components: {
                        app_content: require('../views/debate/single/Single.vue').default,
                    }
                }*/
                //#endregion
            ]
        },
        //#region - auth
        {
            path: '/auth',
            component: require('../layouts/auth/Layout.vue').default,


            children: [
                {
                    path: 'login',
                    component: require('../views/auth/Login.vue').default
                }
            ]

        }
        //#endregion
    ],
});

router.beforeResolve(Auth.isConnected);
