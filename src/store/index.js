import Vue  from 'vue'
import Vuex from 'vuex'
import auth from './modules/auth'
import single from './modules/single'
import singleElement from './modules/singleElement'
import rightMenu from './modules/rightMenu'
import header from './modules/header'
import editData from './modules/editData'
import durations from './modules/durations'
import footer from './modules/footer'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        auth,single,singleElement,header,rightMenu,editData,durations,footer
    }
})
