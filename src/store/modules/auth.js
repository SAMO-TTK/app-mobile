import Vue          from 'vue'
import { router }   from '../../router'
import JWTDecode    from 'jwt-decode'

export default {
    state: {
        isConnected : false,
        token       : null,
        error       : null,

        user        : {},
        permissions : [],
        mfiles      : null
    },

    getters: {

    },

    actions: {
        loadUserState(context) {

            if(!context.state.connected) {

                if(localStorage.token) {

                    let decodedToken = JWTDecode(localStorage.token);

                    if(decodedToken.exp > Math.ceil(Date.now() / 1000)) {
                        context.commit('loginFromLocalStorage', { token: localStorage.token })
                    } else {
                        context.commit('logout')
                    }
                }
            }
        },

        login(context, params) {
            Vue.axios.post(`${process.env.API}/login`, {
                username: params.username,
                password: params.password

            }).then(response => {

                const token = response.data.token;

                return Promise.all([
                    Vue.axios.get(
                        `${process.env.API}/users/${JWTDecode(token).sub}?embed=roles.permissions`,
                        { headers: { Authorization: `Bearer ${token}`}}
                    ),
                    token
                ]);
            }).then(([response, token]) => {

                const user = response.data.data;
                const permissions = [];

                user.roles.forEach(role => {
                    role.permissions.forEach(item => {
                        if(permissions.indexOf(item.id) === -1)
                            permissions.push(item.id);
                    })
                });

                context.commit('loginSuccess', { user, token, permissions })

            }).catch((error) => {
                
                context.commit('loginError', { error: 'Email ou mot de passe incorrect' })
            })
        },
        loginNFC(context, params) {
            Vue.axios.post(`${process.env.API}/nfc`, {
                badge_id : params.badge_id
            }).then(response => {

                const token = response.data.token;

                return Promise.all([
                    Vue.axios.get(
                        `${process.env.API}/users/${JWTDecode(token).sub}?embed=roles.permissions`,
                        { headers: { Authorization: `Bearer ${token}`}}
                    ),
                    token
                ]);
            }).then(([response, token]) => {

                const user = response.data.data;
                const permissions = [];

                user.roles.forEach(role => {
                    role.permissions.forEach(item => {
                        if(permissions.indexOf(item.id) === -1)
                            permissions.push(item.id);
                    })
                });

                context.commit('loginSuccess', { user, token, permissions })

            }).catch((error) => {

                context.commit('loginError', { error: 'Carte inconnu' })
            })
        },
        loginMfiles(context){
            Vue.axios.post(
                `${process.env.MFILES}/server/authenticationtokens`,
                JSON.stringify({
                    Username: "ttkhubitb",
                    Password: "ttkhubitb",
                    VaultGuid: "{7061608C-319D-43C7-9017-6D940C3731C7}"
                })
            ).then(mfilesResponse => {
                let mfiles = mfilesResponse.data.Value.replace('"', '');
                context.commit('loginMfiles', {mfiles});
            }).catch(error => {
                context.commit('loginMfiles', {mfiles:null});
            });
        },

        clearLoginError(context) {
            context.commit('loginError', { error: null });
        },

        logout(context, params) {
            Vue.axios.get(`${process.env.API}/logout`, { headers: { Authorization: 'Bearer ' + params.token } });
            context.commit('logout');
            router.push('/auth/login');
        }
    },

    mutations: {
        loginFromLocalStorage(state, event) {
            let token = JWTDecode(event.token);

            if(token) {
                state.isConnected   = true;
                state.token         = localStorage.token;
                state.user          = {
                    first_name:localStorage.user_first_name,
                    last_name:localStorage.user_last_name,
                    id:localStorage.user_id,
                    avatar:localStorage.user_avatar,
                    wana_id:localStorage.wana_id,
                    badge_id:localStorage.badge_id,
                }
                state.permissions = localStorage.permissions;
                state.mfiles = localStorage.mfiles;

            }
        },

        loginSuccess(state, event) {
            //let decodedToken = JWTDecode(event.token)

            state.token             = event.token;
            state.isConnected       = true;
            state.user.first_name   = event.user.first_name;
            state.user.last_name    = event.user.last_name;
            state.user.id           = event.user.id;
            state.user.avatar       = event.user.avatar;
            state.user.wana_id      = event.user.wana_id;
            state.user.badge_id     = event.user.badge_id;
            state.permissions       = event.permissions;

            localStorage.token              = state.token;
            localStorage.user_first_name    = event.user.first_name;
            localStorage.user_last_name     = event.user.last_name;
            localStorage.user_id            = event.user.id;
            localStorage.user_avatar        = event.user.avatar;
            localStorage.wana_id            = event.user.wana_id;
            localStorage.badge_id           = event.user.badge_id;
            localStorage.permissions        = event.permissions;

        },

        loginMfiles(state, event) {
            state.mfiles = event.mfiles;

            localStorage.mfiles = event.mfiles;
        },

        loginError(state, event) {
            state.error = event.error;
        },

        logout(state) {
            state.token = null;
            state.isConnected = false;
            state.user = {};
            state.permissions = [];
            state.mfiles = null;

            localStorage.clear();
        }
    }
}
