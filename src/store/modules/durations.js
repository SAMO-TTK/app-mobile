import Vue          from 'vue'
import { router }   from '../../router'

export default {
    state: {
        data: {}
    },

    getters: {

    },
    actions: {
        setDurationsData(context,params){
            context.commit('setDurationsData',{ data: params.data})
        },
        clearDurationsData(context) {
            context.commit('clearDurationsData');
        },
        updateDurationsData(context, params) {
            context.commit('updateDurationsData', { key: params.key, value: params.value })
        }
    },

    mutations: {
        setDurationsData(state, event){
            state.data = event.data;
        },
        clearDurationsData(state) {
            state.data = {}
        },
        updateDurationsData(state, event) {
            state.data[event.key] = event.value
        }
    }
}
