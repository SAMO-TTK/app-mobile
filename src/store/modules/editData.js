import Vue          from 'vue'
import { router }   from '../../router'

export default {
    state: {
        data: {}
    },

    getters: {

    },
    actions: {
        setEditData(context,params){
            context.commit('setEditData',{ data: params.data})
        },
        clearEditData(context) {
            context.commit('clearEditData');
        },
        updateEditData(context, params) {
            context.commit('updateEditData', { key: params.key, value: params.value })
        }
    },

    mutations: {
        setEditData(state, event){
            state.data = event.data;
        },
        clearEditData(state) {
            state.data = {}
        },
        updateEditData(state, event) {
            state.data[event.key] = event.value
        }
    }
}
