import Vue          from 'vue'
import { router }   from '../../router'

export default {
    state: {
        data: {}
    },

    getters: {

    },
    actions: {
        setFooterData(context,params){
            context.commit('setFooterData',{ data: params.data})
        },
        clearFooterData(context) {
            context.commit('clearFooterData');
        },
        updateFooterData(context, params) {
            context.commit('updateFooterData', { key: params.key, value: params.value })
        }
    },

    mutations: {
        setFooterData(state, event){
            state.data = event.data;
        },
        clearFooterData(state) {
            state.data = {}
        },
        updateFooterData(state, event) {
            state.data[event.key] = event.value
        }
    }
}
