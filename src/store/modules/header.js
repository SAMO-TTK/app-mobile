import Vue          from 'vue'
import { router }   from '../../router'

export default {
    state: {
        data: {}
    },

    getters: {

    },
    actions: {
        setHeaderData(context,params){
            context.commit('setHeaderData',{ data: params.data})
        },
        clearHeaderData(context) {
            context.commit('clearHeaderData');
        },
        updateHeaderData(context, params) {
            context.commit('updateHeaderData', { key: params.key, value: params.value })
        }
    },

    mutations: {
        setHeaderData(state, event){
            state.data = event.data;
        },
        clearHeaderData(state) {
            state.data = {}
        },
        updateHeaderData(state, event) {
            state.data[event.key] = event.value
        }
    }
}
