import Vue          from 'vue'
import { router }   from '../../router'

export default {
    state: {
        data: {}
    },

    getters: {

    },

    actions: {
        clearRightMenuData(context) {
            context.commit('clearRightMenuData')
        },
        setRightMenuData(context, params) {
            context.commit('setRightMenuData', { data: params.data })
        },
        updateRightMenuData(context, params) {
            context.commit('updateRightMenuData', { key: params.key, value: params.value })
        }
    },

    mutations: {
        clearRightMenuData(state) {
            state.data = {}
        },
        setRightMenuData(state, event) {
            state.data = event.data
        },
        updateRightMenuData(state, event) {
            state.data[event.key] = event.value
        }
    }
}
