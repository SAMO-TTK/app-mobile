import Vue          from 'vue'
import { router }   from '../../router'

export default {
    state: {
        data: {}
    },

    getters: {

    },

    actions: {
        clearSingleData(context) {
            context.commit('clearSingleData')
        },
        setSingleData(context, params) {
            context.commit('setSingleData', { data: params.data })
        },
        updateSingleData(context, params) {
            context.commit('updateSingleData', { key: params.key, value: params.value })
        }
    },

    mutations: {
        clearSingleData(state) {
            state.data = {}
        },
        setSingleData(state, event) {
            state.data = event.data
        },
        updateSingleData(state, event) {
            state.data[event.key] = event.value
        }
    }
}
