import Vue          from 'vue'
import { router }   from '../../router'

export default {
    state: {
        data: {}
    },

    getters: {

    },

    actions: {
        clearSingleElementData(context) {
            context.commit('clearSingleElementData')
        },
        setSingleElementData(context, params) {
            context.commit('setSingleElementData', { data: params.data })
        },
        updateSingleElementData(context, params) {
            context.commit('updateSingleElementData', { key: params.key, value: params.value })
        }
    },

    mutations: {
        clearSingleElementData(state) {
            state.data = {}
        },
        setSingleElementData(state, event) {
            state.data = event.data
        },
        updateSingleElementData(state, event) {
            state.data[event.key] = event.value
        }
    }
}
